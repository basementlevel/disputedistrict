<?PHP
session_start();
require_once("./include/membersite_config.php");
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="dispute_style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>

            function submitForm() {
                document.getElementById('formid').submit();
            }

            function progressBar(x) {
                document.getElementById(team1_progress).style.width = x;
                document.getElementById(team1).innerHtml.value = x;
                document.getElementById(team2_progress).style.width = 100 - x;
                document.getElementById(team2).innerHtml.value = 100 - x;

            }
        </script> 
        <style>
            .well{

                background: lightsteelblue;
            }
        </style>
    </head>
    <body>
        <!-- voting -->

        <h1 ><a id="main" href="disputedistrict.php"> The Dispute District </a>
            <a href="create_poll.php">Get Started! </a>
            <a href="vote.php">Vote! </a>
            <a href="browse.php">View Polls! </a>
        </h1>
        <img src="ddfiles/banner_lmfao.png" alt="Take it to the hole in Miami." class="center">

        <label id="radio-inline1"><input type="radio" name="optradio">Correct Answer</label>
        <button type="button" class="btn btn-success" id ="vote">Vote</button>
        <label id="radio-inline2"><input type="radio" name="optradio">Wrong Answer</label>
        <br>

        <!-- Progress bar -->

        <div class="container" id="progress_container">
            <h2 id="votes_title">Votes are in!</h2>
            <div class="progress">
                <div class="progress-bar progress-bar-success" id="team1_progress"role="progressbar" style="width:50%">
                    <p id="team1"> 50% </p>
                </div>
                <div class="progress-bar progress-bar-warning" id="team2_progress" role="progressbar" style="width:50%">
                    <p id="team2"> 50% </p>
                </div>
            </div>
        </div>

        <!-- Collages -->
        <div class="container" id="collages">           
            <div class="container" id="collage1">
                <div class="row"> 
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div> 
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                </div>

            </div> 
            <div class="container id" id="collage2">
                <div class="row"> 
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div> 
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                    <div class="column">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                        <img src="ddfiles/collage_temp.jpg">
                    </div>
                </div>
            </div>   
        </div> 
        <!-- -->
        <div class="container" id="well_container">
            <div class="well"> 
                <!-- Left-aligned media object -->
                <div class="media" data-toggle="modal" data-target="#voteModal">
                    <div class="media-left">
                        <img src="ddfiles/bliss.jpg" class="media-object" style="width:60px" >
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Paul Giamatti's Daughter4</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>

                <!-- Modal -->
                <div id="voteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content" id="vote-modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>

                                <img id="modal_image" src="ddfiles/collage_temp.jpg">
                                <h4 class="modal-title" id="vote-comment"> This is my comment
                                </h4>
                            </div>
                            <div class="modal-body" id="vote_body">
                                <h4 class="modal-title" id="vote-comment"> This is my comment
                                </h4>

                            </div>
                            <div class="modal-footer">
                                <form action="/action_page.php">

                                    <input type="file" name="pic" accept="image/*">
                                    <textarea id="vote_text"name="message" rows="3" cols="30">
Add comment
                                    </textarea>
                                    <input type="submit">
                                </form>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- Left-aligned media object -->
                <div class="media">
                    <div class="media-left">
                        <img src="ddfiles/soon.png" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Silver Case Face</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>

                <!-- Left-aligned media object -->
                <div class="media">
                    <div class="media-left">
                        <img src="ddfiles/sorry_cory.jpg" class="media-object" style="width:60px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">CORY@HOUSE</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>


            </div></div>
    </body>
</html>