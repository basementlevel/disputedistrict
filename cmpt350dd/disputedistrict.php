<?PHP
ob_start();
session_start();
require_once("./include/membersite_config.php");



if(isset($_POST['submitted']))
{
   if($fgmembersite->RegisterUser())
   {
        $fgmembersite->RedirectToURL("thank-you.html");
   }
} elseif($_POST['submitLogin'])
{

    if($fgmembersite->Login())
    {

    }
}
ob_end_flush();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Dispute District</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="dispute_style.css">  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="jquery-3.3.1.min.js"></script>
  <script src="gen_validatorv4.js" type="text/javascript"></script>
  <script>
      
      function showLogout(param) {
          document.getElementById("logoutDiv").style.display = "inline";
          document.getElementById("logoutText").innerHTML = "Logout ("+param+")";
          document.getElementById("loginRegDiv").innerHTML = "<h3>Welcome back!</h3> Logged in as "+param;
      }
      
      
  
  </script>
  
</head>
<body>

<!--<div class="jumbotron text-center"> -->
  <h1 >The Dispute District
     
  </h1>
  <h1 style="margin-top:0px;background-color:#fff">
     <a href="create_poll.php" class="barLink">Get Started!</a>
     <a href="vote.php" class="barLink">Vote!</a>
     <a href="browse.php" class="barLink">View Polls!</a>
     <div id="logoutDiv" style="display:none"><a href="logout.php" class="barLink"><div id="logoutText" style="display:inline">Logout</div></a></div>
     
  </h1>
<!-- </div> -->

<div id="myCarousel" class="carousel slide" data-ride="carousel" class="center" data-interval="4000">
 

  <!-- Wrapper for slides -->
  <div class="carousel-inner" >
    <div class="item active">
      <img src="ddfiles/banner_lmfao.png" alt="Take it to the hole in Miami." class="center">
    </div>

    <div class="item">
      <img src="ddfiles/banner_saber.png" alt="Another five star??" class="center">
    </div>

    <div class="item">
      <img src="ddfiles/banner_madworld.png" alt="I find it kinda funny, I find it kinda sad." class="center">
    </div>
      
    <div class="item">
      <img src="ddfiles/banner_oldboy.png" alt="The world smiles with you." class="center">
    </div>
  </div>
  
   <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
  
<div class="container" style="margin-top:30px;">
  <div class="row">
    <div class="box col-sm-4">
      <div id ="loginRegDiv">
      <h3>Join the Dispute!</h3>
        
        
      
        <form id='login' action='disputedistrict.php' method='post'
                accept-charset='UTF-8'>
        <fieldset >
        <legend>Login</legend>
        <input type='hidden' name='submitLogin' id='submitLogin' value='1'/>

        <label for='username' >Username:</label>
        <input type='text' name='username' id='username'  maxlength="50" />
        <br/>
        <label for='password' >Password:</label>
        <input type='password' name='password' id='password' maxlength="50" />

        <input type='submit' name='Submit' value='Login' />

        </fieldset>
        </form>
       
        <script type="text/javascript">

        var frmvalidator  = new Validator("login");
        frmvalidator.EnableOnPageErrorDisplay();
        frmvalidator.EnableMsgsTogether();
        frmvalidator.addValidation("username","req","Please provide a username");
        frmvalidator.addValidation("password","req","Please provide a password");
        </script>
        <!-- Trigger/Open The Modal -->
        <button id="myBtn">Don't have an account? Register Now!</button>
        
        </div>

        <!-- The Modal -->
        <div id="myModal" class="modal">

          <!-- Modal content -->
          <div class="modal-content">
            <span class="close">&times;</span>
            <form id='register' action='disputedistrict.php' method='post'
                accept-charset='UTF-8'>
            <fieldset >
            <legend>Register</legend>
            <input type='hidden' name='submitted' id='submitted' value='1'/>
            <label for='name' >Your Full Name: </label>
            <input type='text' name='name' id='name' maxlength="50" />
            <br/>
            <label for='email' >Email Address:</label>
            <input type='text' name='email' id='email' maxlength="50" />
            <br/>
            <label for='username' >Username:</label>
            <input type='text' name='username' id='username' maxlength="50" />
            <br/>
            <label for='password' >Password:</label>
            <input type='password' name='password' id='password' maxlength="50" />
            <input type='submit' name='Submit2' value='Submit' />

            </fieldset>
            </form>
            
            <script type="text/javascript">

            var frmvalidator  = new Validator("register");
            frmvalidator.EnableOnPageErrorDisplay();
            frmvalidator.EnableMsgsTogether();
            frmvalidator.addValidation("name","req","Please provide your name");
            frmvalidator.addValidation("email","req","Please provide your email address");
            frmvalidator.addValidation("email","email","Please provide a valid email address");
            frmvalidator.addValidation("username","req","Please provide a username");
            frmvalidator.addValidation("password","req","Please provide a password");
            </script>
          </div>

        </div>

        <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        
        
        
        </script>
      
    </div>
    <div class="box col-sm-4">
      <h3>Voice Your Opinion!</h3>
     <a href="vote.php">Vote! </a>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget libero eget dui interdum placerat. Sed blandit, justo ut malesuada luctus, erat odio bibendum purus, suscipit efficitur tellus lacus fringilla lorem. Sed nisl quam, venenatis eu bibendum quis, convallis nec mauris. Pellentesque ultricies, velit at lacinia laoreet, ante nibh fermentum turpis, vitae commodo nisi mauris in lectus. Nam blandit porta vulputate. Nam semper urna facilisis mauris suscipit, at sodales odio hendrerit. Maecenas quis scelerisque eros. Sed sed elit sed eros faucibus sodales. Fusce nulla elit, euismod in erat sit amet, blandit convallis sem. Duis suscipit imperdiet nunc, porta porttitor arcu mattis a. Maecenas dapibus vestibulum nulla, a placerat massa elementum eu. Praesent sit amet fringilla nunc. Aenean est purus, condimentum ac magna non, imperdiet posuere ipsum. Vivamus bibendum, lacus eget gravida iaculis, sapien lorem fringilla metus, sed feugiat diam dolor quis massa. Suspendisse maximus lacus bibendum urna malesuada accumsan a quis mi. Cras metus velit, accumsan vitae dolor et, faucibus ullamcorper libero.</p>

    </div>
    <div class="box col-sm-4">
      <h3>Browse Polls!</h3>        
       <a href="browse.php">View Polls! </a>
       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget libero eget dui interdum placerat. Sed blandit, justo ut malesuada luctus, erat odio bibendum purus, suscipit efficitur tellus lacus fringilla lorem. Sed nisl quam, venenatis eu bibendum quis, convallis nec mauris. Pellentesque ultricies, velit at lacinia laoreet, ante nibh fermentum turpis, vitae commodo nisi mauris in lectus. Nam blandit porta vulputate. Nam semper urna facilisis mauris suscipit, at sodales odio hendrerit. Maecenas quis scelerisque eros. Sed sed elit sed eros faucibus sodales. Fusce nulla elit, euismod in erat sit amet, blandit convallis sem. Duis suscipit imperdiet nunc, porta porttitor arcu mattis a. Maecenas dapibus vestibulum nulla, a placerat massa elementum eu. Praesent sit amet fringilla nunc. Aenean est purus, condimentum ac magna non, imperdiet posuere ipsum. Vivamus bibendum, lacus eget gravida iaculis, sapien lorem fringilla metus, sed feugiat diam dolor quis massa. Suspendisse maximus lacus bibendum urna malesuada accumsan a quis mi. Cras metus velit, accumsan vitae dolor et, faucibus ullamcorper libero.</p>
    </div>
  </div>
</div>
<?PHP
      
      if($fgmembersite->CheckLogin())
        {
          echo '<script type="text/javascript">',
          'showLogout("'.$fgmembersite->UserFullName().'");',
          '</script>'
          ;
        }
     ?>
</body>
</html>
